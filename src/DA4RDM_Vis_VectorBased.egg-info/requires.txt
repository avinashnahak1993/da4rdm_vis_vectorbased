plotly>=5.8.2
numpy>=1.18.1
pandas>=1.0.1
python_dateutil>=2.8.2
scipy>=1.4.1
kaleido>=0.2.1
